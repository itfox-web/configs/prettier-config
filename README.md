# `@itfoxweb/prettier-config`

> Itfox prettier [Prettier](https://prettier.io) config.

## Usage

**Install**:

```bash
$ npm install --D @itfoxweb/prettier-config
```

**Edit `package.json`**:

```jsonc
{
  // ...
  "prettier": "@itfoxweb/prettier-config"
}
```
